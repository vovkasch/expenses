FactoryGirl.define do
  factory :image do

  end

  factory :image_invalid, class: Image do
    image { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'fixtures', 'images', 'invalid.jpg')) }
  end

  factory :image_valid, class: Image do
    image { Rack::Test::UploadedFile.new(File.open(Rails.root.join('spec/fixtures/images/test.png'), 'r')) }
  end
end

