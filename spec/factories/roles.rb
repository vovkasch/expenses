FactoryGirl.define do
  factory :role, class: 'Role' do
    name 'guest'

    trait :guest_role do
      name 'guest'
    end

    trait :admin_role do
      name 'admin'
    end

    trait :customer_role do
      name 'customer'
    end

    trait :employee_role do
      name 'employee'
    end
  end
end

