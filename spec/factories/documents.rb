FactoryGirl.define do
  factory :document do
    date Date.today

    trait :new do
      workflow_state 'new'
    end

    trait :in_process do
      workflow_state 'in_process'
    end

    trait :rejected do
      workflow_state 'rejected'
    end

    trait :pending_approval do
      workflow_state 'pending_approval'
    end

  end

end
