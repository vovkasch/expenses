require 'rails_helper'

RSpec.describe CategoriesController, type: :controller do
  let(:category) { create :category, {user_id: 1} }

  describe "whith role #customer" do
    before(:each) do
      sign_in create :user, :customer
    end

    describe "GET #index" do
      it "assigns all categories as @categories" do
        get :index
        expect(assigns(:categories)).to include(category)
      end
    end

    describe "GET #new" do
      it "assigns a new category as @category" do
        get :new
        expect(assigns(:category)).to be_a_new(Category)
      end
    end

    describe "GET #edit" do
      it "assigns the requested category as @category" do
        get :edit, id: category.id
        expect(assigns(:category)).to eq(category)
      end
    end

    describe "POST #create" do
      context "with valid params" do
        it "creates new category" do
          expect {
            post :create, {category: { name: 'test' } }
          }.to change(Category, :count).by(1)
        end

        it "redirects to the created category" do
          post :create, {category: { name: 'test' }}
          expect(response).to redirect_to(categories_path)
        end
      end
    end

    describe "PUT #update" do
      context "with valid params" do
        it "assigns the requested category as @category" do
          put :update, {id: category.to_param, category: { name: 'test' }}
          expect(assigns(:category)).to eq(category)
        end

        it "redirects to the category" do
          put :update, {id: category.to_param, category: { name: 'test' }}
          expect(response).to redirect_to(categories_path)
        end
      end
    end

    describe "DELETE #destroy" do
      it "destroys the requested category" do
        category.save
        expect {
          delete :destroy, {id: category.to_param}
        }.to change(Category, :count).by(-1)
      end

      it "redirects to the categories list" do
        delete :destroy, {id: category.to_param}
        expect(response).to redirect_to(categories_url)
      end
    end
  end

  describe "access to categories" do
    it "employee can not see categories page" do
      sign_in create :user, :employee
      get :index
      expect(response).to redirect_to(root_url)
    end

    it "customer can't get access to other's categories" do
      sign_in create :user, :customer
      others_category_1 = create :category, user: create(:user)
      others_category_2 = create :category, user: create(:user)

      get :index
      expect(assigns(:categories)).not_to include( [others_category_1.id, others_category_2.id] )
    end

    it "admin can see all categories" do
      sign_in create :user, :admin
      get :index
      expect(response).to have_http_status(200)
    end
  end
end
