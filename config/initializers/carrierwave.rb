CarrierWave.configure do |config|
  if Rails.env.production?
    config.storage = :fog
    config.fog_credentials = {
      provider: 'AWS',
      aws_access_key_id: Rails.application.secrets.aws_access_key_id,
      aws_secret_access_key: Rails.application.secrets.aws_secret_access_key,
      region: "eu-west-1"
    }
    config.fog_directory  = "softserve-itacademy-160-expenses"
  else
    config.storage = :file
  end
end

