class Currency < ActiveRecord::Base
  has_one :document
  belongs_to :user
end
