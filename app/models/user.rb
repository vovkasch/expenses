class User < ActiveRecord::Base
  devise :database_authenticatable, :omniauthable, :omniauth_providers => [:facebook, :google_oauth2]
  belongs_to :role
  has_many :currencies
  has_many :categories
  has_many :documents
  before_create :set_default_role

  def self.from_omniauth(auth)
    where(email: auth.info.email).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
      user.name = auth.info.name # assuming the user model has a name
    end
  end

  def role?(role_name)
    role.name == role_name.to_s
  end

  def set_default_role
    self.role ||= Role.find_by_name('guest')
  end
end
