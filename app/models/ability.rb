class Ability
  include CanCan::Ability

  def initialize(user)
    if user.role? :admin
      can :manage, :all
    end

    if user.role? :customer
      can :manage, Image do |image|
        image.try(:user) == user
      end
      can :create, Category
      can :manage, Category do |category|
        category.user == user
      end
      can :create, Document
      can :manage, Document do |document|
        document.try(:user) == user
      end
      can [:approve, :reject], Document
      cannot [:handle, :request_approval, :amend], Document
    end

    if user.role? :employee
      can :update, Document
      can :read, Document
      can [:handle, :request_approval, :amend], Document
    end

    if user.role? :guest
      can :read, Document
    end
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
