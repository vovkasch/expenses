function browse_image(){
  document.getElementById("image_image").click();
};

function submit_form(file_field){
  var size_in_megabytes = file_field.files[0].size/1024/1024;
  if (size_in_megabytes > 5) {
    alert('Image was not submitted. Maximum image size is 5MB. Please choose a smaller file.');
    return false;
  }
  file_field.form.submit();
  return true;
};
