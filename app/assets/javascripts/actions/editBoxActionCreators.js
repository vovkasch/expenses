var Dispatcher = require('../dispatcher/Dispatcher');
var Constants = require('../constants/Constants');

var ActionTypes = Constants.ActionTypes;

module.exports = {
  rotate: function(target_id, angle) {
    Dispatcher.dispatch({
      type: ActionTypes.ROTATE,
      target_id: target_id,
      angle: angle
    });
  },

  save: function(image_id, token) {
    Dispatcher.dispatch({
      type: ActionTypes.SAVE,
      image_id: image_id,
      token: token
    });
  }
};
