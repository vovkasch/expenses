jest.dontMock('../editBoxStore');
jest.dontMock('object-assign');
jest.dontMock('keymirror');

describe('editBoxStore', function() {

  var Dispatcher;
  var editBoxStore;
  var callback;

  var Constants = require('../../constants/Constants');
  var ActionTypes = Constants.ActionTypes;

  var actionRotateLeft = {
    type: ActionTypes.ROTATE,
    target_id: 'rotate_left',
    angle: 0
  };

  var actionRotateRight = {
    type: ActionTypes.ROTATE,
    target_id: 'rotate_right',
    angle: 0
  };

  var actionSave = {
    type: ActionTypes.SAVE,
    image_id: 3,
    token: 'token'
  };

  beforeEach(function() {
    Dispatcher = require('../../dispatcher/Dispatcher');
    editBoxStore = require('../editBoxStore');
    callback = Dispatcher.register.mock.calls[0][0];

  });

  it('registers a callback with the dispatcher', function() {
    expect(Dispatcher.register.mock.calls.length).toBe(1);
  });

  it('rotates image left', function(){
    callback(actionRotateLeft);
    expect(editBoxStore.getState().angle).toEqual(-90);
  });

  it('rotates image right', function(){
    callback(actionRotateRight);
    expect(editBoxStore.getState().angle).toEqual(90);
  });

  it('changes _state.changed to false', function(){
    callback(actionRotateRight);
    expect(editBoxStore.getState().changed).toBe(false);
  });

  it('emits CHANGE event after ROTATE action', function() {
    var $ = require('jquery');
    editBoxStore.emitChange = jest.genMockFunction();

    callback(actionRotateLeft);

    expect(editBoxStore.emitChange).toBeCalled();
  });

  it('calls into $.ajax with the correct params', function() {
    var $ = require('jquery');
    var token = 'token';
    var params = {image_angle: {angle: 0}};

    callback(actionSave);

    expect($.ajax).toBeCalledWith({
      url: "/images/" + actionSave.image_id + "/rotate",
      type: "POST",
      contentType: "application/json",
      xCsrfToken: token,
      data: JSON.stringify(params),
      success: jasmine.any(Function)
    });
  });

  it('calls the ajax callback and sets _state.changed to true', function() {
    var $ = require('jquery');

    callback(actionSave);
    $.ajax.mock.calls[0][0].success();

    expect(editBoxStore.getState().changed).toBe(true);
  });

  it('calls the ajax callback and sets _state.angle to 0', function() {
    var $ = require('jquery');

    callback(actionSave);
    $.ajax.mock.calls[0][0].success();

    expect(editBoxStore.getState().angle).toEqual(0);
  });

  it('emits CHANGE event after SAVE action', function() {
    var $ = require('jquery');
    editBoxStore.emitChange = jest.genMockFunction();

    callback(actionSave);
    $.ajax.mock.calls[0][0].success();

    expect(editBoxStore.emitChange).toBeCalled();
  });

});
