var Dispatcher = require('../dispatcher/Dispatcher');
var Constants = require('../constants/Constants');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var jest = jest || false;
if (!!jest) {
  $ = require('jquery');
}

var ActionTypes = Constants.ActionTypes;
var CHANGE_EVENT = 'change';

var _state = {
  angle: 0,
  changed: true,
  resetImage: false
};

var editBoxStore = assign({}, EventEmitter.prototype, {

  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  getState: function() {
    return _state;
  },

  rotate: function(target_id, angle) {
    var newAngle = target_id == "rotate_left" ? angle - 90 : angle + 90;
    _state.angle = newAngle;
    _state.changed = false;
  },

  save: function(image_id, token){
    var params = {image_angle: {angle: _state.angle}};
    $.ajax({
      url: "/images/" + image_id + "/rotate",
      type: "POST",
      contentType: "application/json",
      xCsrfToken: token,
      data: JSON.stringify(params),
      success: editBoxStore._onChangeAngleSuccess
    });
  },

  _onChangeAngleSuccess: function() {
    _state.changed = true;
    _state.angle = 0;
    _state.resetImage = !_state.resetImage;
    editBoxStore.emitChange();
  }

});

editBoxStore.dispatchToken = Dispatcher.register(function(action) {

  switch(action.type) {

    case ActionTypes.ROTATE:
      editBoxStore.rotate(action.target_id, action.angle);
      editBoxStore.emitChange();
      break;

    case ActionTypes.SAVE:
      editBoxStore.save(action.image_id, action.token);
      break;
  }
});

module.exports = editBoxStore;
