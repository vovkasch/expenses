var localeFrom = function(source) {
  return source.match(/locale=\w+/)[0].split('=')[1];
};
I18n.locale = localeFrom(document.cookie);
