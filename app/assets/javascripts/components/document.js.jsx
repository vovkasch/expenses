var StateBox = React.createClass({
  getInitialState: function() {
    return {
      currentState: this.props.currentState,
      nextStates: JSON.parse(this.props.nextStates),
    }
  },

  handleClick: function(event, nextState) {
    $.ajax({
      url: "/documents/" + this.props.documentId + "/" + event,
      dataType: 'json',
      type: 'PUT',
      success: function(data) {
        this.setState({currentState: nextState, nextStates: data});
      }.bind(this),
      error: function (xhr, status, err) {
        console.error("/documents/" + this.props.documentId + "/" + event, status, err.toString());
      }.bind(this)
    });
  },

  render: function() {
    var statesButtons = this.state.nextStates.map(function (event) {
      var handleClick = this.handleClick.bind(this, event.name, event.transitions_to);
      return (
        <button onClick = {handleClick} className = "btn btn-primary btn-state">
          <span className = "glyphicon glyphicon-arrow-right"></span>
          {I18n.t("edit.state." + event.transitions_to)}
        </button>
      );
    }, this);

    return (
      <div className = "state-box">
          <h4 className = "well well-sm" id = "current_state">
            {I18n.t("edit.current_state")} <br /> {I18n.t("edit.state." + this.state.currentState)}
          </h4>
          {statesButtons}
      </div>
    );
  }
});
