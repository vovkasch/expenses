var editBoxActionCreators = require('../actions/editBoxActionCreators');
var editBoxStore = require('../stores/editBoxStore');

function getStateFromStores() {
  return editBoxStore.getState();
}

var EditBox = React.createClass({

  componentDidMount: function(){
    var EditBox = this;
    $(".zoomContainer").ready(function() {
      EditBox.setZoom();
    });
    editBoxStore.addChangeListener(this._onChange);
  },

  getInitialState: function() {
    return {
      angle: 0,
      changed: true,
      resetImage: false
    }
  },

  setZoom: function() {
    $("#" + this.props.image_id).elevateZoom({
      zoomType : "lens",
      lensShape : "round",
      lensSize : 200,
      containLensZoom : true
    });
   },

  render: function() {
    var imgStyle = { transform: 'rotate(' + this.state.angle + 'deg)' };
    var change = this.props["update"] == 'true' ? false : true;
    return (
      <div className = "image-div span12" id = "inner_edit">
        <div className = "image-div span12" id = "inner_image">
          <img
            id = {this.props["image_id"]}
            className = {this.props["class"]}
            src = {this.props["src"] + '?' + this.state.resetImage + new Date().valueOf().toString() }
            style = {imgStyle}
            data-zoom-image = {this.props["data"]}></img>
        </div>
        <div className = "upload-edit">
          <button onClick = {browse_image} id = "change" className = "btn btn-primary" disabled = {change}> {I18n.t("edit.button_change")} </button>
          <button onClick = {this._rotate} id = "rotate_left" className = "btn glyphicon glyphicon-repeat"></button>
          <button onClick = {this._rotate} id = "rotate_right" className = "btn glyphicon glyphicon-repeat"></button>
          <button onClick = {this._save} id = "save" className = "btn btn-primary" disabled = {this.state.changed}> {I18n.t("edit.button_save")} </button>
        </div>
      </div>
    );
  },

  _onChange: function() {
    var state = getStateFromStores();
    this.setState(state);
  },

  _rotate: function(event) {
    $(".zoomContainer").remove();
    editBoxActionCreators.rotate(event.target.id, this.state.angle);
  },

  _save: function() {
    var image = $("#" + this.props.image_id)[0];
    var token = document.getElementById("save_token").children[1].content;
    editBoxActionCreators.save(this.props.image_id, token);
    image.src = "";
    this.setZoom();
    var newImage = $("#" + this.props.image_id)[0];
    var EditBox = this;
    var id = setInterval(function() {
      if (parseInt($(".zoomContainer").css('width')))
        clearInterval(id);
      $(".zoomContainer").css({width: newImage.offsetWidth, height: newImage.offsetHeight});
      $(".zoomLens").css('background-image', "url(" + EditBox.props.data + '?' + new Date().valueOf().toString() + ")");
    }, 100);
  }
});

module.exports = EditBox;
