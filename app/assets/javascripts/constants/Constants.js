var keyMirror = require('keymirror');

module.exports = {
  ActionTypes: keyMirror({
    ROTATE: null,
    SAVE: null
  })
};
