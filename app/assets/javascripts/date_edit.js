$(document).on('ready page:load', function(event) {
  $(function() {

    $.datepicker.setDefaults(
      $.extend(
        $.datepicker.regional[I18n.locale.toString()]
      )
    );

    $("#document_date").datepicker({
      dateFormat:'d MM yy',
      showOtherMonths: true,
      selectOtherMonths: true,
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      yearRange: '2010:2040'
    });

    if (I18n.locale != I18n.defaultLocale)
      document_date.value = localizeMonthView(document_date.value, true);

    update_document.addEventListener("mouseup", function(event) {
      if (I18n.locale != I18n.defaultLocale)
      document_date.value = localizeMonthView(document_date.value);
      event.stopPropagation();
    });

  });
});

var localizeMonthView = function(dateString, origin) {
  var localized = dateString.replace(/\b\D+\b/, function(mach) {
    var originMonths = $.datepicker.regional[I18n.defaultLocale].monthNames
    var currentLocaleMonths = $.datepicker.regional[I18n.locale].monthNames;
    var month
    if (origin) {
      month = currentLocaleMonths[originMonths.indexOf(mach.trim())];
    } else {
      month = originMonths[currentLocaleMonths.indexOf(mach.trim())];
    };
    return (' ' + month + ' ');
  });
  return localized
};
