class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authenticate_user!, except: :sign_in
  before_action :set_locale

  def after_sign_in_path_for(*args)
    documents_url
  end

  def after_sign_out_path_for(*args)
    sign_in_url
  end

  rescue_from CanCan::AccessDenied do |exception|
    access_denied(exception)
  end

  def access_denied(exception)
    redirect_to root_url, flash: {error: exception.message}
  end

  def set_locale
    set_locale_cookie if params[:locale]
    if cookies[:locale]
      I18n.locale = cookies[:locale]
    else
      I18n.locale = params[:locale] || I18n.default_locale
    end
  end

  def set_locale_cookie
    cookies[:locale] = params[:locale]
  end

  def default_url_options(options = {})
    { locale: I18n.locale }.merge options
  end

  def check_freshness(record)
    unless Rails.env.test?
      response.etag = combine_etags({ etag: record })
      response.etag << bind_to_session(response.etag)
      head :not_modified if request.fresh?(response)
    end
  end

  def bind_to_session(etag)
    "#{session['session_id'].slice(3..20)}\""
  end
end
