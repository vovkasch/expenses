class ImagesController < ApplicationController

  # POST /images
  def create
    @collection = save_collection image_params[:image]
    notice = notice @collection
    respond_to do |format|
      if @collection.size == 1 && !@collection[0].errors.any?
        format.html { redirect_to edit_document_path(@collection[0].document), notice: 'Document was created.' }
      else
        format.html { redirect_to documents_path, notice: notice.join("<br/>") }
      end
    end
  end

  def rotate
    image = Image.find params[:id]
    authorize! :rotate, image
    image.rotate_image(params[:image_angle]["angle"])
    respond_to do |format|
      format.js {}
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def image_params
    params.require(:image).permit({image: []})
  end

  def save_collection(collection)
    collection.map { |image| Image.create({image: image}.merge(user_id: current_user.id)) }
  end

  def notice(collection)
    collection.map do |image|
      if image.errors.any?
        image.errors.messages[:image].map { |e| "#{image.filename} - rejected - #{e}" }
      end
    end.flatten.compact
  end
end
