class DocumentsController < ApplicationController
  before_action :set_document, except: [:index, :new]
  after_action :update_document, only: [:handle, :request_approval, :approve, :reject, :amend]
  load_and_authorize_resource

  # GET /documents
  # GET /documents.json
  def index
    @documents = Document.includes(:image).available_documents(current_user).page(params[:page])
    check_freshness @documents
  end

# GET /documents/new
  def new
    image = Image.create(user_id: current_user.id)
    @document = image.document
    render 'edit'
  end

  # GET /documents/1/edit
  def edit
    @next_states = @document.aviliable_states(current_ability).to_json
    check_freshness @document
  end

  # PATCH/PUT /documents/1
  # PATCH/PUT /documents/1.json
  def update
    respond_to do |format|
      if @document.update(document_params)
        format.html { redirect_to documents_url}
      else
        format.html { render :edit }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    @document.destroy
    respond_to do |format|
      format.html { redirect_to documents_url, notice: 'Document was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def handle
    authorize! :handle, @document
    @document.handle!
    respond_to do |format|
      format.json { render json: @document.aviliable_states(current_ability) }
    end
  end

  def request_approval
    authorize! :request_approval, @document
    @document.request_approval!
    respond_to do |format|
      format.json { render json: @document.aviliable_states(current_ability) }
    end
  end

  def approve
    authorize! :approve, @document
    @document.approve!
    respond_to do |format|
      format.json { render json: @document.aviliable_states(current_ability) }
    end
  end

  def reject
    authorize! :reject, @document
    @document.reject!
    respond_to do |format|
      format.json { render json: @document.aviliable_states(current_ability) }
    end
  end

  def amend
    authorize! :amend, @document
    @document.amend!
    respond_to do |format|
      format.json { render json: @document.aviliable_states(current_ability) }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_document
      @document = Document.find(params[:id])
    end

    def update_document
      Document.update(@document.id, updated_at: Time.now)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def document_params
      params.require(:document).permit(:date, :amount, :currency_id, :category_id, :description, image_attributes: [:id ,:image] )
    end
end
