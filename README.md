[![Build Status](https://semaphoreci.com/api/v1/projects/db41124e-72ed-454a-91b8-71b3b1010999/540434/badge.svg)](https://semaphoreci.com/ljowe/expenses)

[![Coverage Status](https://coveralls.io/repos/softserveacademy160/expenses/badge.svg?branch=master&service=bitbucket)](https://coveralls.io/bitbucket/softserveacademy160/expenses?branch=master)

## Hot to create a new branch: ##

git checkout master && git pull origin master && git checkout -b feature/LVRUB-1_my_new_feature

## How to push that branch: ##

git push origin feature/LVRUB-1_my_new_feature:feature/LVRUB-1_my_new_feature
